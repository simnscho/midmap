# midmap

Map between manifestation ID (BV number), copy/item ID (shelfmark) and digitized image version ID (pid) for library objects.
List child objects of a digitized object. (images of a series)

## Scenario

The catalogue of the Bibliotheksverbund Bayern lists titles (LRM manifestations) with an assigned ID (BV number).
The local catalogue of the university library lists copies (LRM items) with an assigned shelfmark as ID.
The digitool web portal of digitized images of the university collections assigns persistent identifiers ("pid"s) to identify single images as
well as image series/digitized objects.

The current library systems APIs don't allow for an easy mapping of these IDs. This service tries to provide a
concise interface by accessing some (undocumented) APIs and extracting the relevant information.

## Usage

- http://dhlab.ub.fau.de/midmap/bv/:bvnumber:
- http://dhlab.ub.fau.de/midmap/shm/:shelfmark:
- http://dhlab.ub.fau.de/midmap/pid/:pid:

The services return a json array with nested objects, where each object contains a valid 
combination of BVID, shelfmark and the pid:

```
[
  {
    "bv":"XXX",
    "shm":"XXX",
    "pid":"XXX"
  },
  {
    "bv":"XXX",
    "shm":"XXX",
    "pid":"XXX"
  },
  ...
]
```

- http://dhlab.ub.fau.de/midmap/children/:pid:

The service returns a a json array with the pids of all the child objects for the given digitized object pid. 
Each child object represents a single image while the parent object represents the whole image series.

[ "XXX", "XXX", ...]


## Used APIs and config

The service uses these APIs:

1. Touchpoint's SRW-API, in Erlangen accessible via http://uertp20.bib-bvb.de:8080/SRW/search/

  This API contains information for mapping shelfmarks and BV numbers to the other IDs

2. Digitool's item viewer web page

  This page contains a link containing the BV number, if the digitized item has been catalogued.

