
/* **
 * CONFIGURATION
 * **/

/* 
 * These constants can be set be environment variables; but default is a reasonable value
 */
// unless the env is defined, we won't use https 
const USE_HTTPS   = (typeof process.env.USE_HTTPS !== 'undefined') && !(/^(false|no|off|0)$/.test(process.env.USE_HTTPS));
const PORT        = process.env.PORT        || 28777;
const PORT_HTTPS  = process.env.PORT_HTTPS  || 28771;
const CRED_PRIV   = process.env.CRED_PRIV;
const CRED_CERT   = process.env.CRED_CERT;

/*
 * URLs for fetching data; should normally not be changed
 */
const TP_URL_BV       = "http://uertp20.bib-bvb.de:8080/SRW/search/?query=dc.bvbid%3D%22${bv}%22&version=1.1&operation=searchRetrieve&recordSchema=info%3Asrw%2Fschema%2F1%2Fmarcxml-v1.1&maximumRecords=100&startRecord=1"
const TP_URL_SHM      = "http://uertp20.bib-bvb.de:8080/SRW/search/?query=dc.anywhere+exact+%22${shm}%22&version=1.1&operation=searchRetrieve&recordSchema=info%3Asrw%2Fschema%2F1%2Fmarcxml-v1.1&maximumRecords=100&startRecord=1&resultSetTTL=300&recordPacking=xml&recordXPath=&sortKeys="
const DT_URL_PAGE     = "http://digipool.bib-bvb.de/bvb/delivery/metadata.fpl?pid=${pid}"
const DT_URL_CHILDREN = "http://digital.bib-bvb.de/view/bvbmets/getStructMap.jsp?pid=${pid}&parta=DE-29&au=BAY01"

/*
 * Namespaces for processed docs; cannot and should not be configured
 */
const namespaces = {
  "marc": "http://www.loc.gov/standards/marcxml/schema/MARC21slim.xsd",
  "srwmarc": "info:srw/schema/1/marcxml-v1.1"
};

/*
 * Defaults for returned fields; defaults may be overridden by query params
 */
const returnFieldsDefaults = {
  bv: true,
  shm: true,
  pid: true,
  cpid: false
};


/* **
 * IMPORTS / REQUIRED MODULES
 * **/

const express   = require('express');
const fs        = require('fs');
const xmldom    = require('xmldom');
const xpath     = require('xpath').useNamespaces(namespaces);
const fetch     = require('node-fetch');


/* **
 * SERVER SETUP
 * **/
const app = express();
var http = require('http');
var serverHttp = http.createServer(app);
var serverHttps = null;
console.log('Using HTTP' + (USE_HTTPS ? 'S' : '') + ' env is ' + process.env.USE_HTTPS);
if (USE_HTTPS) {
  var https = require('https');
  var privateKey  = fs.readFileSync(CRED_PRIV, 'utf8');
  var certificate = fs.readFileSync(CRED_CERT, 'utf8');
  var credentials = {key: privateKey, cert: certificate};
  serverHttps = https.createServer(credentials, app);
}


/* **
 * ROUTING FUNCTIONS
 * **/

/* Routing table:
 * /bv/:bv                     GET       Get ID mappings by BV number
 * /shm/:shm                   GET       Get ID mappings by shelfmark
 * /pid/:pid                   GET       Get ID mappings by pid
 * /children/:pid              GET       Get pids of children (images) of container pid
 */

/** Handler for adding CORS support
 *
 * see also IIIF manifest store: https://github.com/textandbytes/iiif-manifest-store
 */
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    
  // intercept OPTIONS method
  if ('OPTIONS' == req.method) {
    res.sendStatus(200);
  }
  else {
    next();
  }
});


/** Route handler for requests by BV number 
 *
 */
app.route('/bv/:bv').get(function(req, res, next) {
  var bv = req.params.bv;
  if (! /^BV\d+$/.test(bv)) {
    console.error("The BV number has a bad format.");
    throw new Exception("The BV number has a bad format.");
  }
  console.log('Got request with BV number ', bv);  
  
  var url = app.substitutePlaceholders(TP_URL_BV, {'bv': bv});
  fetch(url)
    .then(result => result.text())
    .then(xml => app.extractFromTpSrw(xml))
    .then(record => res.json(record))
    .catch(err => next(err));
});


/** Route handler for requests by shelfmark
 *
 */
app.route('/shm/:shm').get(function(req, res, next) {
  var shm = req.params.shm;
  if (! /^\w+\/.+$/.test(shm)) {
    console.error("The shelfmark has a bad format.");
    throw new Exception("The shelfmark has a bad format.");
  }
  console.log('Got request with shelfmark ', shm);
  
  var url = app.substitutePlaceholders(TP_URL_SHM, {'shm': shm});
  fetch(url)
    .then(result => result.text())
    .then(xml => app.extractFromTpSrw(xml))
    .then(record => res.json(record))
    .catch(err => next(err));
});


/** Route handler for requests by PID
 *
 */
app.route('/pid/:pid').get(function(req, res, next) {
  var pid = req.params.pid;
  if (! /^\d+$/.test(pid)) {
    console.error("The PID has a bad format.");
    throw new Exception("The PID has a bad format.");
  }
  console.log('Got request with PID ', pid);
  
  var url = app.substitutePlaceholders(DT_URL_PAGE, {'pid': pid});
  fetch(url)
    .then(result => result.text())
    .then(htmlFragment => app.extractFromDigitoolPage(pid, '<div>' + htmlFragment + '</div>'))
    .then(record => res.json(record))
    .catch(err => next(err));
});


/** Route handler for requests for child PIDs
 *
 */
app.route('/children/:pid').get(function(req, res, next) {
  var pid = req.params.pid;
  if (! /^\d+$/.test(pid)) {
    console.error("The PID has a bad format.");
    throw new Exception("The PID has a bad format.");
  }
  console.log('Got request for children with PID ', pid);
  
  var url = app.substitutePlaceholders(DT_URL_CHILDREN, {'pid': pid});
  fetch(url)
    .then(result => result.json())
    .then(json => app.getChildPids(json))
    .then(childPids => res.json(childPids))
    .catch(err => next(err));
});




/* **
 * HELPER FUNCTIONS
 * **/


/** Replace placeholders of the form ${placeholder} in a string
 *
 * If ${placeholder} is not in map, the ${} is discarded but the placeholder is not substituted
 */
app.substitutePlaceholders = function(str, map) {
  return str.replace(/\$\{(\w+)\}/g, (match, p1) => { return map[p1] || p1; });
};


/** Extract the fields to be returned from the URLs query params
 *
 * NOTE: currently not used
 */
app.getReturnFieldsFromQuery = function(query) {
  // there are default values for the fields in the const declaration section
  var tokens = query.hasOwnProperty('fields')
               ? (Array.isArray(query.fields)
                 ? query.fields
                 : query.fields.split(/\W+/)
               )
               : Object.keys(query);
  tokens = tokens.filter(t => returnFieldsDefaults.hasOwnProperty(t));
  if (!tokens) {
    // if no fields are explicitly given, we return default values
    return returnFieldsDefaults;
  }
  else {
    var fields = {};
    Object.keys(returnFieldsDefaults).forEach(f => fields[f] = tokens.includes(f));
    return fields;
  }
}


/** Find records in a MARC xml file and extract all relevant information
 *
 */
app.extractFromTpSrw = function(srwMarcXml) {
  //console.log('bla', srwMarcXml);
  var dom = (new xmldom.DOMParser()).parseFromString(srwMarcXml);
  var records = xpath('//srwmarc:record', dom);
  var mappings = [];
  records.forEach(record => {
    app.aggregateMappingFromMarcRecord(record).forEach(m1 => {
      // sort out duplicates
      var add = true;
      mappings.forEach(m2 => {
        if (m1.bv == m2.bv && m1.shm == m2.shm && m1.pid == m2.pid) add = false;
      });
      if (add) mappings.push(m1);
    });
  });
  return mappings;
}


/** Get BV number, shelfmark and PID from a single MARC xml record
 *
 */
app.aggregateMappingFromMarcRecord = function(record) {
  var bvs = Array
    .from(xpath('//marc:datafield[@tag="035"]/marc:subfield[@code="a" and starts-with(., "(DE-604)BV")]', record))
    .map(e => e.textContent.substring(8));
  var shms = Array
      .from(xpath('//marc:datafield[@tag="983"]/marc:subfield[@code="j"]', record))
      .map(e => e.textContent);
  var pids = Array
      //.from(xpath('//marc:datafield[@tag="856" and marc:subfield/@code="z"]/marc:subfield[@code="u" and contains(text(), "pid=")]', record))
      .from(xpath('//marc:datafield[@tag="856"]/marc:subfield[@code="u" and contains(text(), "pid=")]', record))
      .map(e => e.textContent.replace(/^.*((?<=pid=)\d+).*$/, "$1"));
  return app.makeCrossProduct(bvs, shms, pids);
};


/** Extract BV number and shelfmark from a digitool page
 *
 */
app.extractFromDigitoolPage = function(pid, html) {
  var dom = (new xmldom.DOMParser()).parseFromString(html);
  var mappings = [];
  var bvs = Array
    .from(xpath('//a[contains(@href, "gateway-bayern.de/")]', dom))
    .map(e => e.textContent.replace(/^.*\/(BV\d+).*$/, "$1"));
  var shms = Array
    .from(xpath('//div[text() = "Signatur"]', dom))
    .map(e => e.nextSibling.textContent);
  return app.makeCrossProduct(bvs, shms, [pid]);
}


/** Calculate the cross product between a list of BV numbers, shelfmarks, and PIDs
 *
 * The resulting list contains all possible combinations of BV number, shelfmark, and PID.
 */
app.makeCrossProduct = function(bvs, shms, pids) {
  // create explicit empty values for empty fields 
  bvs  = bvs.length ? bvs : [''];
  shms = shms.length  ? shms  : [''];
  pids = pids.length  ? pids  : [''];
  // filter out duplicates
  bvs. filter((v, i, s) => { return s.indexOf(v) === i; });
  shms.filter((v, i, s) => { return s.indexOf(v) === i; });
  pids.filter((v, i, s) => { return s.indexOf(v) === i; });
  // make the cross product
  var product = [];
  bvs.forEach(bv => {
    shms.forEach(shm => {
      pids.forEach(pid => {
        product.push({ bv: bv, shm: shm, pid: pid });
      });
    });
  });
  // filter out duplicates
  product.filter((v, i, s) => { return s.indexOf(v) === i; });
  return product;
}; 


/** Fetch the child pids for a work pid
 *
 */
app.getChildPids = function(json) {
  function collectChildPidsRecursively(json, cpids) {
    if (Array.isArray(json)) {
      json.forEach(i => collectChildPidsRecursively(i, cpids));
    }
    else {
      if (json.attr.pid) {
        cpids.push(json.attr.pid);
      }
      if (json.children) {
        json.children.forEach(i => collectChildPidsRecursively(i, cpids));
      }
    }
    return cpids;
  };
  return collectChildPidsRecursively(json, []);
};


/* **
 * START SERVER
 * **/
serverHttp.listen(PORT, function () {
  console.log('Midmap started listening on port ' + PORT + " for access with http");
});
if (serverHttps) {
  serverHttps.listen(PORT_HTTPS, function () {
    console.log('Midmap started listening on port ' + PORT_HTTPS + " for access with https");
  });
}
